import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int num;
        int numFactorial = 1;

        try {
            System.out.println("Input an integer whose factorial will be computed ");
            num = in.nextInt();

            if (num == 0) {

                System.out.println("The factorial of " + num + " is " + numFactorial);
            }
            else if (num > 0) {

                /*for (int i = 2; i <= num; i++) {

                    numFactorial *= i;
                }*/

                int numInitial = num;
                while (numInitial != 0) {

                    numFactorial *= numInitial;
                    numInitial --;
                }

                System.out.println("The factorial of " + num + " is " + numFactorial);
            }
            else {

                System.out.println("Cannot compute for factorials of negative numbers. ");
            }
        }
        catch (Exception e) {

            System.out.println("You input is invalid, please input a valid number");
            e.printStackTrace();
        }
    }
}